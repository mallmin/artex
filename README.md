# artex

Hello! This repository contains media and code to share with the team working on bringing ecological community dynamics to life for ARTEX.

## How to make abundance plots

Run the python script ``abundance_graphs.py`` to produce graphs of abundance time series. ```python abundance_graphs.py --help``` explains how the script works: 

```
usage: abundance_graphs.py [-h] [-S S] [--mu MU] [--sig SIG] [--lam LAM] [--gam GAM] [--weak] [-T T] [--t0 T0] [--X0 X0] [--seed SEED] [--title TITLE] [--file-name FILE_NAME] [--log-scale] [-n N]

Generates abundance time series from a simulation of a Lotka-Volterra community with random interactions. Graphs are stored as .png in the same folder as the script.

optional arguments:
  -h, --help            show this help message and exit
  -S S                  number of species (default: 100)
  --mu MU               mean interaction strength (default: 0.5)
  --sig SIG             standard deviation of interaction strength (default: 0.3)
  --lam LAM             rate of immigration (default: 0.0)
  --gam GAM             correlation of ij and ji interaction (default: 0.0)
  --weak                rescale interactions by number of species (default: False)
  -T T                  length of recorded trajectory (default: 500)
  --t0 T0               length of discarded transient (default: 0)
  --X0 X0               total initial abundance (individual abundances random uniform) (default: 1.0)
  --seed SEED           seed for random number generator (default: None)
  --title TITLE         title of plots (default: test)
  --file-name FILE_NAME
                        name of file (but will be auto-formatted) (default: same as TITLE)
  --log-scale           apply log scale to line graph (default: False)
  -n N                  number of species shown in line graph (default: S)
```
