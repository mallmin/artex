import os, sys
sys.path.append('./')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import matplotlib.pyplot as plt
import argparse
import glv


### Parse arguments ###

# desc = "Generates abundance time series from a simulation of a Lotka-Volterra community with random interactions. Graphs are stored as .png in the same folder as the script.\
# For MU and SIG around 0.1 to 1., one can find persistent chaos or cycles if LAM is not zero (a tiny amount 1e-8 sufficies). If MU > 1 one should find competitive exclusion \
# if SIG is small, otherwise more complicated dynamics can occurr. If SIG is ever too large the community diverges in abundance. If MU and SIG are appropriate re-scaled by\
# the community size (use --weak flag) one can find the unique fixed point phase if SIG < root(2) and MU > -1."
desc = "Generates abundance time series from a simulation of a Lotka-Volterra community with random interactions. Graphs are stored as .png in the same folder as the script."

parser = argparse.ArgumentParser(description=desc, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-S", type=int, default=100, help="number of species")
parser.add_argument("--mu", type=float, default=0.5, help="mean interaction strength")
parser.add_argument("--sig", type=float, default=0.3, help="standard deviation of interaction strength")
parser.add_argument("--lam", type=float, default=0., help="rate of immigration")
parser.add_argument("--gam", type=float, default=0., help="correlation of ij and ji interaction")
parser.add_argument("--weak", action="store_true", help="rescale interactions by number of species")
parser.add_argument("-T", type=int, default=500, help="length of recorded trajectory")
parser.add_argument("--t0", type=int, default=0, help="length of discarded transient")
parser.add_argument("--X0", type=float, default=1., help="total initial abundance (individual abundances random uniform)")
parser.add_argument("--seed", type=int, default=None, help="seed for random number generator")
parser.add_argument("--title", type=str, default="test", help="title of plots")
parser.add_argument("--file-name", type =str, default="same as TITLE", help="name of file (but will be auto-formatted)")
parser.add_argument("--log-scale", action="store_true", help="apply log scale to line graph")
parser.add_argument("-n", default="S", help="number of species shown in line graph")

args = parser.parse_args()

args.n = min(args.S if args.n == "S" else int(args.n), args.S)
if args.title == "":
    args.title = f"Regime: mu={args.mu}, sig={args.sig}, lam={args.lam}"
mu = args.mu
sig = args.sig
if args.weak:
    mu /= args.S
    sig /= np.sqrt(args.S)
if args.file_name == "same as TITLE":
    args.file_name = args.title.replace('-','_')
args.file_name = args.file_name.replace(' ','_').lower()

## Set up simulation ##

sim = glv.DisorderedLVSystem(S=args.S, mu=mu, sig=sig, gam=args.gam, lam=args.lam, alpha_seed=args.seed)
sim.initialize_uniform(seed=args.seed)
sim.vec_x *= args.X0 / np.sum(sim.vec_x)

## Generate trajectory ##

traj = sim.trajectory(args.T*100, transient = 100*args.t0, record_interval=100)

if np.any(np.isnan(traj.mat_x)):
    print("Community diverged! (Aborting)")
    quit()

## Plot and save figures ##

plt.rc('font', size=16)
plt.rc('axes', titlesize=12)
colors = traj.colors(seed=args.seed)
DPI = 200

param_str = f"$S = {args.S}\ " + \
    (f"\mu={args.mu}/S\ \sigma={args.sig}/\sqrt{{S}}\ " if args.weak else f"\mu={args.mu}\ \sigma={args.sig}\ " ) \
    + f"\gamma={args.gam}\ \lambda=${args.lam}\n" + f"$t_0 = {args.t0}\ X_0 = {args.X0}\ $seed = {args.seed}"

# stackplot
fig, ax = plt.subplots()
ax.stackplot(traj.vec_t, traj.mat_x.T, colors=colors)
ax.set_xlabel("time")
ax.set_ylabel("stacked abundances")
fig.suptitle(args.title, y=0.92)
ax.set_title(param_str)
fig.tight_layout()
file_name = args.file_name + "_STACK.png"
fig.savefig(subpath(file_name), dpi=DPI)

# line plot
fig, ax = plt.subplots()
for i in range(args.S):    
    color = "lightgrey" if i < args.S - args.n else colors[i]
    ax.plot(traj.vec_t, traj.mat_x[:,i], color=color)
if args.log_scale:
    ax.set_yscale("log")
ax.set_xlabel("time")
ax.set_ylabel("abundances")
fig.suptitle(args.title, y=0.92)
ax.set_title(param_str)
file_name = args.file_name + "_LINES.png"
fig.tight_layout()
fig.savefig(subpath(file_name), dpi=DPI)
