'''
This file contains utilities for curve fitting
'''

import numpy as np
from scipy.optimize import minimize_scalar


def coeff_det(vec_obs, vec_pred):
    '''
    Coefficient of determination.

    Attributes
    ----------
    vec_obs
        observed data series
    vec_pred
        predicted data series
    '''
 
    SSres = np.linalg.norm(vec_obs - vec_pred)**2
    SStot = np.linalg.norm(vec_obs - np.mean(vec_obs))**2
    r2 = 1 - SSres/SStot
    return r2


def linear_fit(vec_x, vec_t=None):
    '''
    Best linear fit via least squares.

    Attributes
    ----------
    vec_x
        value of the function at time points
    vec_t
        time values of time point (assumed 0,1,2,... if not given)

    Returns
    -------
    lf
        object with c (constant), k (slope), and r2 (goodness of fit)
    
    '''

    if vec_t is None:
        vec_t = np.arange(vec_x.size)
    coeffs = np.polynomial.polynomial.polyfit(vec_t, vec_x, 1)

    # class LinearFit():
    #     def __init__(self, c,k,r2):
    #         self.c = c # constant
    #         self.k = k # slope
    #         self.r2 = r2

    r2 = coeff_det(vec_x, coeffs[0] + coeffs[1]*vec_t)
    # lf = LinearFit(coeffs[0], coeffs[1], r2)

    lf = {'c' : coeffs[0], 'k' : coeffs[1] , 'r2' : r2 } #should be picklable

    return lf

    # c = coeffs[0]
    # k = coeffs[1]
    # r2 = coeff_det(vec_x, c + k*vec_t)
    # return [c,k], r2


def exponential_decay_fit(vec_x, vec_t=None, bounds=(1e-3,1000)):
    '''Fits an exponential decay model: exp(-t/tau)
     
     Attributes
    ----------
    vec_x
        value of the function at time points
    vec_t
        time values of time point (assumed 0,1,2,... if not given)
    bounds
        bounds on the decay rate

    Returns
    -------
    edf
        object with , and r2 (goodness of fit)
        
    '''
    if vec_t is None:
        vec_t = np.arange(vec_x.size)
    else:
        vec_t -= vec_t[0] #shift t0 to 0
    
    def cost_fun(tau):
        vec_exp = np.exp(- vec_t / tau)
        return np.sum( (vec_exp - vec_x) **2 )  
   
    # class ExponentialDecayFit():
    #     def __init__(self, tau, r2):
    #         self.tau = tau
    #         self.r2 = r2

    tau = minimize_scalar(cost_fun, bounds=bounds, method='bounded').x
    r2 = coeff_det(vec_x, np.exp(- vec_t / tau))

    # edf = ExponentialDecayFit(tau, r2)

    edf = {'tau' : tau, 'r2' : r2}

    return edf

# def average_decay_rate(vec_x, vec_t, start=1):
#     '''
#     Estiamates average rate of decay tau.
    
#     tau defined by
#         exp(- theta(t)t) = f(t)/f(0)

#         tau = 1/ avg(theta)

#     Assumes vec_t is evenly spaced    
#     '''
#     vec_t = vec_t - vec_t[0]
#     vec_x = vec_x / vec_x[0]
   
#     theta_avg = - np.mean(np.log(vec_x[start:]) / vec_t[start:])
#     print(theta_avg)

#     tau = 1 / theta_avg

#     r2 = coeff_det(vec_x, np.exp(- vec_t / tau))

#     return  tau, r2
