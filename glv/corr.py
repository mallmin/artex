'''
This file contains functions for computing auto- and crosscorrelation functions,
on both CPU and GPU. The GPU gain for this type of computation is massive.

The by far fastest implementation is via fast Fourier transform (FFT) on GPU,
by which all crosscorrelations of O(100) variables in O(1000) time points is 
computed in a few minutes. The same algorithm can also run on CPU. For auto-
correlation, a version using direct compution approach and one using scipy are
included as reference cases, to validate the preferred FFT implementation.
'''

import sys
import numpy as np

def autocorr_fft(arr, axis=0, lags=200, standardize=True, real_input=True, target='cpu'):
    '''
    Calculate the autocorrelation function using FFT on cpu or gpu.

    Parameters
    ----------
    arr : ndarray
        discrete time series
    axis : int
        axis along which autocorrelation is computed 
    lags : int
        number of lags to include (size of return vector)
    standardize : bool
        to subtract mean and normalize by variance
    target : {'cpu', 'gpu'}
        platform to run FFT on

    Returns
    -------
    vec_acf : ndarray
        transformed array, where the ``axis`` dimension indexes time lags in range ``0:lags``    
    '''
    # Zero padding for FFT
    pow2 = int(2**np.ceil(np.log2(arr.shape[axis])))

    if standardize:
        arr = arr - np.mean(arr,axis=axis)

    
    if target == 'gpu':
        if 'cupy' not in sys.modules:
             import cupy
        xp = sys.module['cupy']
        arr = xp.asarray(arr, dtype='float32')
        
    else:
        xp = np

    fft = xp.fft.rfft if real_input else xp.fft.fft
    ifft = xp.fft.irfft if real_input else xp.fft.ifft

    arr_fft = fft(arr, axis=axis, n=pow2)
    arr_acf = ifft( arr_fft * xp.conjugate(arr_fft), axis=axis).real / pow2
    
    # Normalize
    if standardize:
        ax = arr.ndim - 1 if axis == -1 else axis
        ids = (slice(None),)*ax + (0,) + (slice(None),)*(arr.ndim - ax - 1)
        arr_acf = arr_acf[:lags] / arr_acf[ids]
   
    if target == 'gpu':
        arr_acf = xp.asnumpy(arr_acf)

    return arr_acf



def crosscorr_fft(mat, lags=200, standardize=True, real_input=True, target='cpu', progress=False):
    '''
    Compute all crosscorrelation functions between columns of input.

    Parameters
    ----------
    mat : 2darray
        time x replicate array
    axis : int
        axis along which autocorrelation is computed 
    lags : int
        number of lags to include (size of return vector)
    standardize : bool
        to subtract mean and normalize by variance
    target : {'cpu', 'gpu'}
        platform to run FFT on
        
    Returns
    -------
    ten_ccf : 3darray
        transformed array, where the dimension 0 indexes time lags in range ``0:lags``,
        and dimension 1,2 the indices of columns of the input
    '''

    # Zero padding for FFT
    pow2 = int(2**np.ceil(np.log2(mat.shape[0])))
    S = mat.shape[1]  

    if standardize:
        mat = mat - np.mean(mat,axis=0)
  
    if target == 'cpu':
        dtype = np.float64
        xp = np        
    elif target=='gpu':
        if 'cupy' not in sys.modules:
            import cupy
        dtype = np.float32
        xp = sys.modules['cupy']
        mat = xp.asarray(mat, dtype=dtype)
    
    ten_ccf = xp.zeros((lags,S,S), dtype=dtype)

    fft = xp.fft.rfft if real_input else xp.fft.fft
    ifft = xp.fft.irfft if real_input else xp.fft.ifft

    mat_fft = fft(mat, axis=0, n=pow2)

    for i in range(S):
        if progress: print(f"Progress {int(100*i/S)}%", end='\r')
        
        vec_fft_i = xp.conjugate(mat_fft[:,i])[:,None]
        
        #column j now represents the (i,i+j) correlator
        mat_ccf = ifft(mat_fft[:,i:] * vec_fft_i, axis=0).real / pow2
                
        ten_ccf[:,i,i:] = mat_ccf[:lags,:]
    
    if standardize:
        vec_std = xp.diagonal(ten_ccf[0,:,:])
        mat_var = xp.sqrt(xp.outer(vec_std, vec_std))

        # could give silent division by zero => nan
        ten_ccf /= mat_var

    # fill in lower triangle
    ten_ccf += xp.transpose(ten_ccf, axes=(0,2,1)) #will double the diagonal
    
    # Halve the diagonal. Maybe a smarter way exists?
    mat_halve_diag = xp.ones((S,S),dtype=dtype)
    xp.fill_diagonal(mat_halve_diag, 0.5)
    ten_ccf *= mat_halve_diag

    if target == 'gpu':
        ten_ccf = xp.asnumpy(ten_ccf)
    
    if progress: print("Progress 100%")

    return ten_ccf



def avg_autocorr_direct(mat, lags=200):
    '''
    Slow but correct: reference to check correctness of faster implementations
    '''

    N = mat.shape[0]    
    vec_acf = np.zeros(lags)
    mat = mat - np.mean(mat, axis=0)
    for k in range(lags):
        for i in range(0,N-k):
            vec_acf[k] += np.mean(mat[i, :] * mat[i + k, :])
        vec_acf[k] /= N - k
    vec_acf /= vec_acf[0]

    return vec_acf


def avg_autocorr_scipy(mat, lags=200, method='auto'):
    '''
    Reference to see if custom implementation is faster or not
    '''

    from scipy.signal import correlate, correlation_lags

    N = mat.shape[0]
    S = mat.shape[1]

    lag_idxs = correlation_lags(N,N)

    mat = mat - np.mean(mat, axis=0)
    vec_acf = np.zeros(lags)
    for i in range(S):
        vec_corr = correlate(mat[:,i],mat[:,i], method=method)
        vec_corr /= vec_corr[lag_idxs[0]]
        vec_acf += vec_corr[lag_idxs[:lags]]
    vec_acf /= S
    return vec_acf