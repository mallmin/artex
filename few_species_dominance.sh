# If we increase the varaince of interactions compared to a cmopetitive exclusion scenario, it is possible to find a small set of coexisting species that outcompete all others.

python abundance_graphs.py -S 100 --mu 1.1 --sig 0.6 --lam 1e-8 --t0 0 -T 500 -n 20 --seed 2 --log-scale --title "Few-species dominance"