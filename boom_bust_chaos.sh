# When interactions are sufficiently strong and diverse (but not too extreme), 

python abundance_graphs.py -S 300 --mu 0.6 --sig 0.4 --lam 1e-8 --t0 5000 -T 500 -n 20 --seed 2 --log-scale --title "Boom-bust chaos"