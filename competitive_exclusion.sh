# When competition between species is generally stronger than competition with same-species indivduals, ie MU > 1 and SIG small, the competitive exclusion principle states that a single species will eventually outcompete all others.

python abundance_graphs.py -S 100 --mu 1.5 --sig 0.2 --lam 1e-8 --t0 0 -T 300 -n 20 --seed 1 --log-scale --title "Competitive exclusion"